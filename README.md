# PhiloMath JavaScript BootCamp curriculum

## 1. Master Git and Version control.

#### Course: 
https://www.udacity.com/course/version-control-with-git--ud123

#### Video Tutorial:
https://www.youtube.com/watch?v=8JJ101D3knE

## 2. HTML and CSS Basics Tutorial.

#### How HTML & CSS work together
https://blog.codeanalogies.com/2018/05/09/the-relationship-between-html-css-and-javascript-explained/

#### Course:
https://learn.shayhowe.com/html-css/
##### - Mandatory sections to be covered:
Lesson 1: Building Your First Web Page
Lesson 2: Getting to Know HTML
Lesson 3: Getting to Know CSS
Lesson 4: Opening the Box Model
Lesson 5: Positioning Content - Skip positioning with floats

https://learn.shayhowe.com/advanced-html-css/
##### -Madatory sections to be covered:
Lesson 4: Responsive Web Design
##### -Stretch goals:
Lesson 7: Transforms
Lesson 8: Transitions & Animations

#### Video Tutorial
https://www.youtube.com/watch?v=vQWlgd7hV4A

## 3. CSS Precedence
http://tutorials.jenkov.com/css/precedence.html

## 4. Flexbox with drills and videos 
#### -Cheat sheet
https://css-tricks.com/snippets/css/a-guide-to-flexbox/

#### -Play Game and learn
https://flexboxfroggy.com/

#### -Videos
##### CSS Flexbox in 100 Seconds
https://www.youtube.com/watch?v=K74l26pE4YA

##### Flexbox CSS in 20 minutes
https://www.youtube.com/watch?v=JJSoEo8JSnc

##### Course
https://flexbox.io/

## 5. Grid with drills and videos
#### -Cheat sheet
https://grid.malven.co/
https://css-tricks.com/snippets/css/complete-guide-grid/

#### -Play game and learn
https://cssgridgarden.com/

#### -Course
https://cssgrid.io/

## 6 JavaScript
#### Course:
https://www.codecademy.com/learn/introduction-to-javascript

# Tasks

### Task-1 on Flex
#### url
https://gitlab.com/_vaibhav_mehta/4-cards-task-1
#### Solution: 
https://drive.google.com/file/d/1ZnFWkHOpFGLLsqwP7ZJ33ZQkgTyXbIbf/view?usp=sharing

### Task-2 on Grid
#### url
https://gitlab.com/_vaibhav_mehta/task-2
#### Solution
https://drive.google.com/file/d/125u7qjTed1CQIltu9Rn1c5rQKzKp0Y95/view?usp=sharing

### Task-3 Create a Startup Landing Page should be 100% mobile friendly
#### Solution Task3 (Part1)
https://drive.google.com/file/d/1oJsG6_8AYJKLvlTLDJdyuULf_N8_09FF/view?usp=sharing
#### Solution Task3 (Part2)
https://drive.google.com/file/d/11s5dVxIQpsXQoSCSmvEdcFmEaUqixP7P/view?usp=sharing

